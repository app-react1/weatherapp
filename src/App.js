import React, { Fragment, useState, useEffect } from 'react';
import Header from './components/Header';
import Form from './components/Form';
import Weather from './components/Weather';
import Error from './components/Error';

function App() {

  // Create state
  const [search, setSearch] = useState({
    city: '',
    country: ''
  });

  // State from API
  const [consult, setConsult] = useState(false);

  // State for data from API
  const [result, setResult] = useState({});

  // State for error 404 from request API
  const [error, setError] = useState(false);

  // Get country and city
  const { city, country } = search;

  useEffect(() => {
    const consultApi = async () => {
      if (consult) {
        const appId = '89c41e57945244dd45044dc77e11a103';
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${appId}`;

        const response = await fetch(url);
        const result = await response.json();

        (result.cod === '404') ? setError(true) : setError(false);

        setResult(result);
        setConsult(false);
      }
    }
    consultApi();
    // eslint-disable-next-line
  }, [consult]);

  let component;
  (error)
    ? component = <Error message="No se encontró la ciudad" />
    : component = <Weather
      result={result}
    />;


  return (
    <Fragment>
      <Header
        title='Clima React App'
      />

      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Form
                search={search}
                setSearch={setSearch}
                setConsult={setConsult}
              />
            </div>
            <div className="col m6 s12">
              {component}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
